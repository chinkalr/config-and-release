cd /opt/jenkins/workspace/FAB-TEST-RELEASE
REGION=`cat environments/$environmentFile.json | jq -r '.region'`
REPOSITORY_NAME=` cat environments/$environmentFile.json | jq -r '.repositoryName'`
CLUSTER=` cat environments/$environmentFile.json| jq -r  '.clusterName'`
FAMILY=` cat environments/$environmentFile.json| jq -r  '.taskDefinitionFamily '`
NAME=` cat environments/$environmentFile.json| jq -r  ' .taskDefinitionName'`
SERVICE_NAME=` cat environments/$environmentFile.json| jq -r  ' .serviceName'`
COUNT=1
min=` cat environments/$environmentFile.json | jq -r '.minimumHealthyPercent' `
alb=` cat environments/$environmentFile.json | jq -r '.loadBalancerName' `

echo -e " Region: ${REGION}"
echo -e " Repository: ${REPOSITORY_NAME}"
echo -e " Family : ${FAMILY}"
echo -e " Task Name: ${NAME}"
echo -e " Service Name : ${SERVICE_NAME}"

SERVICES=` su -l root -c ' aws ecs describe-services --region '${REGION}' --cluster '${CLUSTER}' --services  '${SERVICE_NAME}' | egrep  "DEPLOYMENTS" | awk "{print \$3}" | wc -l ' `

echo -e "Services Are : $SERVICES"

INACTIVE_STATE=` su -l root -c 'aws ecs describe-services --region '${REGION}'  --cluster '${CLUSTER}' --services  '${SERVICE_NAME}'  | grep 'INACTIVE' |wc -l ' `

echo -e "Is Inactive : $INACTIVE_STATE"



TASK_REVISION=` su -l root -c 'aws ecs describe-task-definition --region '${REGION}' --task-definition '${NAME}' | egrep -i 'TASKDEFINITION' | awk "{print \\$4}" '  `

#TASK_REVISION=` su -l root -c 'aws ecs describe-task-definition --region '${REGION}' --task-definition '${NAME}' | egrep -i 'TASKDEFINITIONARN'   | awk "{print \$2}"  | tr -d "\"\,"  | cut -d '/' -f2 | cut -d ':' -f2 '`

echo -e "Task Revision is : $TASK_REVISION"
echo -e "INACTIVE_STATE $INACTIVE_STATE"
echo -e "SERVICES $SERVICES"

if [ ${SERVICES} -eq 0 ] ; then

     echo "create new service"
     su -l root -c  ' aws ecs create-service --region '${REGION}' --cluster '${CLUSTER}' --service-name  '${SERVICE_NAME}' --task-definition '${FAMILY}':'${TASK_REVISION}' --desired-count '${COUNT}' --health-check-grace-period-seconds 300 '
else
	if [ $INACTIVE_STATE -ne 0   ] ; then
		su -l root -c ' aws ecs create-service --region '${REGION}' --cluster '${CLUSTER}' --service-name '${SERVICE_NAME}' --task-definition '${FAMILY}':'${TASK_REVISION}' --desired-count '${COUNT}' --health-check-grace-period-seconds 300 ' 

	else
		su -l root -c  'aws ecs update-service --region '${REGION}' --cluster '${CLUSTER}' --service  '${SERVICE_NAME}' --task-definition '${FAMILY}':'${TASK_REVISION}' --desired-count '${COUNT}' --deployment-configuration minimumHealthyPercent='${min}' ' 
 
	fi

fi