cd /opt/jenkins/workspace/FAB-TEST-RELEASE
JsonPath=`cat environments/$environmentFile.json | jq -r '.tasktemplatejson'`
REPOSITORY_NAME=` cat environments/$environmentFile.json | jq -r '.repositoryName'`

echo "Json Path is: ${JsonPath}"

if [ $environmentFile = "DEV" ] || [ $environmentFile = "QA" ] || [ $environmentFile = "UAT"   ] || [ $environmentFile = "PERF"   ];then 

	sed -i -e  's/\("image":\)\(.*\)/"image": "029921894534.dkr.ecr.us-east-1.amazonaws.com\/'$REPOSITORY_NAME':'$imagesTag'",/' ${JsonPath}

fi

if [ $environmentFile = "DR" ] ; then 
	sed -i -e  's/\("image":\)\(.*\)/"image": "531593016994.dkr.ecr.us-east-2.amazonaws.com\/'$REPOSITORY_NAME':'$imagesTag'",/' ${JsonPath}

fi
