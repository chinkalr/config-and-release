cd /opt/jenkins/workspace/FAB-TEST-RELEASE
REGION=`cat environments/$environmentFile.json | jq -r '.region'`
REPOSITORY_NAME=` cat environments/$environmentFile.json | jq -r '.repositoryName'`
CLUSTER=` cat environments/$environmentFile.json| jq -r  '.clusterName' `
#NAME=fab-task
NAME=` cat environments/$environmentFile.json| jq -r  '.taskDefinitionName' `
#SERVICE_NAME=fab-svc
SERVICE_NAME=` cat environments/$environmentFile.json| jq -r  '.serviceName' `
#BUILD_NUMBER=10
FAMILY=`cat environments/$environmentFile.json | jq -r '.taskDefinitionFamily' `
JSONPATH=`cat environments/$environmentFile.json | jq -r '.tasktemplatejson' `
echo -e " Region: $REGION"
echo -e "Repository Name: $REPOSITORY_NAME"
echo -e "Cluster Name: $CLUSTER"
echo -e "Task Name : $NAME "
echo -e "Task Family : $FAMILY"
echo -e "Json Path : $JSONPATH"

sed -e "s;%BUILD_NUMBER%;${BUILD_NUMBER};g" ${JSONPATH} > wubsfabfinal.json-v_${BUILD_NUMBER}.json
#sed -e "s;IMAGE;${imagesTag};g" ${JSONPATH} > wubsfabfinal.json-v_${BUILD_NUMBER}.json
#sudo chown -R root:root  wubsfabfinal.json-v_${BUILD_NUMBER}.json
cat wubsfabfinal.json-v_${BUILD_NUMBER}.json
cp  wubsfabfinal.json-v_${BUILD_NUMBER}.json /opt/jenkins/workspace/FAB-TEST-RELEASE/environments
#cd /opt/jenkins/workspace/FAB-TEST-RELEASE/environments
#chown root.root /opt/jenkins/workspace/FAB_Deploy/wubsfabfinal.json-v_${BUILD_NUMBER}.json
su -l root -c ' cd /opt/jenkins/workspace/FAB-TEST-RELEASE/environments && aws ecs register-task-definition --family '$FAMILY' --cli-input-json file://wubsfabfinal.json-v_'${BUILD_NUMBER}'.json '